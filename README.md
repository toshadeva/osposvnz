# osposvnz

script that given a mail will deploy a release and configure the opensourcepos web app POS system for VenenuX cloud4info

script que dado un correo desplegara y configurara el sistema opensourcepos web app POS para el sistema VenenuX cloud4info

## Requisitos, instalacion y desarrollo

Este escript depende de un servicio de correo funcionando, bash y php, tiene dos partes, 
el backend o programas que hacen el trabajo, y el frontend o formularios web para el usuario.

El formulario toma los datos y los envia al backend donde con estos se ejecuta la tarea 
de desplegar unuevo projecto para ser usado por este usuario.

Para entender esto lease el documento [INSTALACION.md](INSTALACION.md)

## Desarrollo

Si piensa participar en el desarrollo es obligatorio VenenuX, Devuan o Debian, o Alpine linux.
Lease el documento [Desarrollo.md](DESARROLLO.md) para configurar su Debian/VenenuX 
o lease el documento [Desarrollo-alpine.md](Desarrollo-alpine.md) pra el Alpine linux.

* Una vez con su entorno abra uan terminal, y coloquese en el directorio `Devel`, esto es obligatorio, 
* Clone su repositorio alli y abra el editor geany o gambas para codificar.
* Terminado suba sus cambios al repositorio en una rama nueva o en su forlk
