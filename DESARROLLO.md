# GUIA PARA ENTORNO DESARROLLO DEBIAN

## 1 instalar el OS

Esta guia empleela con los siguientes:

* Debian, 8 jessie
   * AntiX, 17.0, 17.3 (jessie) 
   * MXLinux 18.0
* Debian, 9 stretch
   * AntiX, 18.3
* Debian, 10 buster
   * Antix 19
   * MXLinux 19

**DEVUAN** en el caso de Devuan debera cambiar 
las referencias en los repos, tenga cuidado.

**IMPORTANTE** Para los novatos, mejor usar un sistema limpio, 
nada de servicios de web, ni impresion ni correo ni nada extra.

## 2 configurar repositorios

**NOTA IMPORTNTE DEVUAN** usar `http://deb.devuan.org/merged` en ves de los url Debian 
en el caso de usar Devuan .. CUIDADO con esto.

```
apt-get --no-install-recommends install base-files lsb-release

rm -f /etc/apt/sources.list

cat > /etc/apt/apt.conf.d/50venenuxcustom << EOF
APT::Get::AllowUnauthenticated "true";
Acquire::AllowInsecureRepositories "true";
Acquire::Check-Valid-Until "false";
EOF

cat > /etc/apt/sources.list.d/50debianoficial.list << EOF
deb  http://ftp.us.debian.org/debian/ $(lsb_release -s -c) main contrib non-free
deb  http://ftp.ca.debian.org/debian/ $(lsb_release -s -c)-security main contrib non-free
deb  http://ftp.us.debian.org/debian/ $(lsb_release -s -c) main contrib non-free
deb  http://ftp.ca.debian.org/debian/ $(lsb_release -s -c)-updates main contrib non-free
EOF

cat > /etc/apt/sources.list.d/50debianbackports.list << EOF
deb   http://ftp.us.debian.org/debian/ $(lsb_release -s -c)-proposed main contrib non-free
deb   http://ftp.us.debian.org/debian/ $(lsb_release -s -c)-backports main contrib non-free
EOF

apt-get update
```

## 3 configurar el OS base

Estos son soporte base para el funcionamiento del sistema asi como archivos comunes para los servicios:

```
apt-get -y install aptitude csh \
   wget less groff bzip2 lrzip lzop lsof linux-base ca-certificates curl \
   perl perl-base perl-modules fam libfam0 apt-transport-https \
   python mime-support libsqlite3-0

apt-get -y install openssh-server openssh-client

openssl req -x509 -days 1460 -nodes -newkey rsa:4096 \
   -subj "/C=VE/ST=Bolivar/L=Upata/O=VenenuX/OU=Systemas:hozYmartillo/CN=localhost" \
   -keyout /etc/ssl/certs/localhost.pem -out /etc/ssl/certs/localhost.pem
chmod 640 /etc/ssl/certs/localhost.pem
chowm daemon:www-data /etc/ssl/certs/localhost.pem


sed -i -r 's#.*Port.*#Port 19090#g' /etc/ssh/sshd_config
sed -i -r 's#.*PermitRootLogin.*#PermitRootLogin no#g' /etc/ssh/sshd_config
sed -i -r 's#.*X11Forwarding.*#X11Forwarding no#g' /etc/ssh/sshd_config

```

Lo que se realizo es asegurar ciertos paquetes base, generar el certificado, 
y configurar el disco para que ejecute mas rapido.

Adicional se cerro el ssh solo a un puerto especifico y sin acceso de admin.

Los siguientes se necesitan para monitoreo, gestion y solucion de posibles y futuros problemas, 
permite mantener monitoreo y analisis de red, disco y procesos:

```
apt-get install ttyrec multitail dtach tmux byobu atop htop atsar dstat powertop calcurse

apt-get install ipcal socat netpipes mtr fatrace slurm aria2 rsync curl
```

La primera linea de instalacion es para programas de gestion y monitoreo de recursos 
de la maquina, disco y sockets, asi como grabacion de la terminal (si necesitas recordar
alguna secuencia de comandos) como calculo de ip y redes

La segunda linea es completamente para redes e internet, `aria2` es especial mencion 
ya que soporta la descarga por multiples protocolos.. es el descargador masivo con 
todos los features de red necesario para el trabajo.


## 5 Instalacion de servicios


#### 1 - INSTALACION DEVCOP

Esto es soporte para compilar y desarrollo o soprote para backend, 
por si tenemos que **compilar algun programa extra no en los repos, 
adicional soprote para gestion y analisis de red asi como internet**.

Abrir una consola, y ejecutar "su" para cambiar al usuario root, 
recuerde que no usamos winbuntu, una vez como root ejecutar los 
siguientes comandos, cada secuencia o comando 
que debe ejecutar esta separado por una linea de espacio:

**NOTA EN DEVUAN EL COMANDO `$(lsb_release -s -c)` no devolvera lo esperado, 
sustituir por 9 ("strecht") o 8 ("jessie") o 10 ("buster") segun la version de Devuan**

```
apt-get purge --force-all mariadb* mysql*

cat > /etc/apt/apt.conf.d/50venenuxcustom << EOF
APT::Get::AllowUnauthenticated "true";
Acquire::AllowInsecureRepositories "true";
Acquire::Check-Valid-Until "false";
EOF

echo 'deb http://download.opensuse.org/repositories/home:/vegnuli:/deploy-1.0/Debian_$(lsb_release -sr | cut -d. -f1).0/ /' > /etc/apt/sources.list.d/home:vegnuli:deploy-1.0.list

wget -nv https://download.opensuse.org/repositories/home:vegnuli:deploy-1.0/Debian_$(lsb_release -sr | cut -d. -f1).0/Release.key -O "/etc/apt/trusted.gpg.d/home:vegnuli:deploy-1.0.asc"

apt-get update 

apt-get install build-essential devscripts gambas3 golang lua5.1 lua5.2 liblua5.1-dev liblua5.2-dev sqlite3 sqliteman

apt-get install git tig subversion mercurial meld diff grep ack less cloc siege tsung iftop iptraf iptraf-ng
```

Todo lo anterior fue para programacion y soporte de repositorios de control de codigo.
especial mencion a `tig` el cual es para la consola, permite ver los logs mas visualmente, 
de lso repositorios git, `subversion` y `mercurial` estan para que no haga falta ya 
que otros proyectos pueden tener dependencias no deseadas.

Se agrego `siege` y `tsung` para prubas de stress y analisis de trafico en el caso de sockets o web devel.

Los programas `meld` y `sqliteman` son un gestor grafico de diff y un gestor de sqlite, 
estos seran muy utiles para manejar archivos de parches y base de datos sqlite.

#### 2 - INSTALACION LAMP

Nosotros no usamos basura windosera, empleamos lighttpd, 
apache es para lso flojos ignorantes ademas de ser ineficiente.

Abrir una consola, y ejecutar "su" para cambiar al usuario root, 
recuerde que no usamos winbuntu, en esos casos es "sudo", 
una vez como root ejecutar los siguientes comandos, cada secuencia o comando 
que debe ejecutar esta separado por una linea de espacio:

**NOTA EN DEVUAN EL COMANDO `$(lsb_release -s -c)` no devolvera lo esperado, 
sustituir por "strecht" o "jessie" o "buster" segun la version de Devuan**

```
apt-get purge --force-all mariadb* mysql*

cat > /etc/apt/apt.conf.d/50venenuxcustom << EOF
APT::Get::AllowUnauthenticated "true";
Acquire::AllowInsecureRepositories "true";
Acquire::Check-Valid-Until "false";
EOF

cat > /etc/apt/sources.list.d/51-mysql.list << EOF
deb http://repo.mysql.com/apt/debian/ $(lsb_release -s -c) mysql-5.6
deb http://repo.mysql.com/apt/debian/ $(lsb_release -s -c) mysql-5.7
deb http://repo.mysql.com/apt/debian/ $(lsb_release -s -c) mysql-tools
EOF

apt-get update

apt-get -y --force-yes install mysql-server mysql-client mysql-common mysql-community-server

apt-get -y --force-yes install lighttpd spawn-fcgi apache2-utils

apt-get -y --force-yes install php php-common php-cgi php-fpm php-cli \
  php-curl php-mbstring php-mysql php-bcmath php-xml php-xmlrpc
```

Despues de un rato de descargar, segun la version de Debian 
habra instalado el software de programas LAMP (lighttp, Mysql y PHP), 
ahora hay que integrar sus configuraciones al servidor web, 
ejecutando los siguientes comandos separados cada uno por una linea vacia:

```
sed -i 's/bind-address.*=.*/bind-address=127.0.0.1/g' /etc/mysql/mysql.conf.d/mysqld.cnf

/usr/sbin/service  mysql restart

/usr/sbin/lighty-enable-mod accesslog fastcgi-php dir-listing status userdir usertrack

sed -i -r 's|userdir.path.*=.*|userdir.path = "Devel"|g' /etc/lighttpd/conf-available/10-userdir.conf

for i in $(ls /home);do mkdir -p -m 775 /home/$i/Devel ;done

for i in $(ls /home);do touch /home/$i/Devel/.noborrar ;done

echo "<?php echo phpinfo();" > /var/www/html/infophp.php

rm  /var/www/html/index.lighttpd.html

/usr/sbin/service lighttpd restart
```

Esto configura el webserver nuestro directorio de documentos, el directorio "Devel" 
pueda contener archivos de servicio web y php sin tener que ser root:

Probamos el servidor web y nuestro directorio colocando una tilde en la direccion del nombre de usuario, 
por ejemplo el `/home/usuario/Devel` sera en el navegador `http://127.0.0.1/~usuario/` 


#### 2 - INSTALACION HERRAMIENTAS IDE GUI

Ya tenemos en equivalente a entorno DEVCOP  (backend) Y LAMP (webserver, php, mysql), 
ahora necesitamos el editor de texto para codigo fuente y 
el gestor de cambios y control de versiones:

Abrir una consola, y ejecutar "su" para cambiar al usuario root, 
Como root ejecutar los siguientes comandos, cada secuencia o comando 
que debe ejecutar esta separado por una linea de espacio:

```
apt-get --ignore-missing -y install geany geany-plugins giggle git meld  mysql-workbench
```

**NOTA** Si el comando resulto en error que no existe el paquete "mysql-workbench" 
repita pero borrando "myql-workbench", sucede solo en Debian 10, para este 
descargar de la pagina web de oracle el de ubuntu 18 e instalar 
en el Debian 10 con `dpkg -i mysql-workbench*.deb` desde el directorio donde se 
descargo.

Despues de instalar tendra listo para ejecutar desde el menu "Desarrolo" :

**IMPORTANTE** NO EJECUTAR ESTOS PASOS COMO ROOT SINO COMO USUARIO NORMAL su usuario.

1. Abrir una consola de linux normal
2. cambiarse al directorio "Devel" asi:
3. Descargar el proyecto o clonar con git en Devel asi: `git clone <url>`
4. sustituya el `<url>` por la direccion del proyecto o sino use un directorio para su proyecto


El proyecto estara ya listo para empezar codificar, 
(la siguiente seccion enseña como), **se puede verificar en el navegador, 
usando la ruta:**

## Usando Geany

El IDE geany nos da todo lo necesario incluso navegacion web, no necesitamos 
de un navegador pesadisimo como firefox o chrome, para esto debemos configurar 
los dos plugins mas importantes: "treebrowser" y "webhelper".

El geany tiene tres regiones principales, tambien movibles aunque 
inicialmente viene asi:

1. Area de codificacion, es el area principal superior y a la derecha.
2. Area de archivos, es el area secundario, superior y a la iquierda.
3. Area de mensajes y web, es el area de multiusos, en el inferior.

Despues debemos configurar en el codigo Codeigniter nuestro proyecto, y ver 
como se ven nuestros cambios en el IDE geany.

![x-guides-codeigniter-grocerycrud-debian-13-usando-ci-geany.png](x-guides-codeigniter-grocerycrud-debian-13-usando-ci-geany.png)

Habilitar el plugin de "treebrowser" para ver archivos del proyecto en contexto:

1. Abrimos el editor geany desde el menu, 
2. En la barra superior, en "Herramientas" abrimos el apartado "Administracion de complementos", 
3. Navegamos hacia el final y vemos el complemento "Visor de arbol"
4. Activamos la casilla y vemos que aparece al lado una nueva pestaña

En esa pestaña nueva es que estableceremos el directorio "Devel", foto:

![x-guides-codeigniter-grocerycrud-debian-14-usando-ci-geany-plugin-tree.png](x-guides-codeigniter-grocerycrud-debian-14-usando-ci-geany-plugin-tree.png)

Abrimos el proyecto desde el area de archivos

* 1 - Para ver el arbol de proyecto debemos navegar entre las pestañas, 
estas aparecen con flechas hacia los lados para activar 
cada pestaña necesitada, la que se necesita es la de "Visor de arbol"
* 2 - Puede introducir la ruta de su directorio donde esta el proyecto 
en la direccion directamente en donde esta ubicado los archivos.
* 3 - Al ubicar el proyecto se pueen navegar los archivos para ubicar 
en un proyecto, y estos son los archivos a comenzar modificar:
* 4 - al terminar de configurar ya podra "probar" su proyecto en el mismo 
servidor web acabado de instalar, como "127.0.0.1" en el navegador tal cual 
se mostro en las imagenes anteriores, sin necesidad de ser root.

![x-guides-codeigniter-grocerycrud-debian-15-configurando-ci-geany.png](x-guides-codeigniter-grocerycrud-debian-15-configurando-ci-geany.png)

## Usando MySQL workbench

Abriremos el mysqlworkbench para poder crear la base de datos que 
usaremos.

1. En el menu superior seleccionar el de "Database", porque no viene en español.
2. Despues seleccionar el elemento "Manage Connections" para crear la conexcion.
3. Al abrirse el cuadro de dialogo pulsar el boton "new" abajo en la esquina.
4. Colocar el nombre por ejemplo "localhost" arriba
5. Seleccionar "local pipe/Socket" en el tipo de conexcion debajo del nombre.
6. En el ruta/host colocar "/var/run/mysql/mysql.sock" para casi todos los debian.
7. colocar el nombre de usuario, generalmente root ya que es local en su maquina.

![x-guides-codeigniter-grocerycrud-debian-17-configurando-ci-mysql-workbench.png](x-guides-codeigniter-grocerycrud-debian-17-configurando-ci-mysql-workbench.png)
## Entendiendo un desarrollo consola

El funcionamiento de todo sistema digital no es web, es en el sistema operativo, 
y en este se ejecutan LOS VERDADEROS programas, UNA WEB NO ES UN PROGRAMA.

Los programas se compilan, para eso se codifican, el lenguaje mas famoso es C y java, 
pero hoy dia se emplea mucho golang, y C#

Digamos el siguiente codigo C:

```
#include <stdio.h>

int main (int argc,char **argv)
{
   printf("algo\n");
}
```

* Se crear un archivo de texto plano `algo.c` y se escribe el codigo 
* compilamos el programa con la linea `gcc algo.c -o algo`
* se ejecuta este archivo llamandolo con `./algo` y se vera lo que realiza


## Entendiendo a un desarrollo web

El funcionamiento es "yo pido en el navegador" -> "el servidor responde si el controlador existe", 
este "servidor responde" se le llama "response" y la peticion 
que realiza "yo pido en el navegador" se le llama "request", 
a este par se le denomina en desarrollo coo "peticon - respuesta" o 
mejor denominado "request - response" (mal llamado cliente servidor).

Un ejemplo y recomendado es Codeigniter, maneja estas peticiones en los "controller" que estan 
en `applications/controller/` cada archivo es un controllador unico, 
y cada controlador es uan peticion, entonces

* tenemos un controlador "welcome" en `appliactions/controller/welcome.php`
* en el navegador para llamarlo se usa `http://127.0.0.1/~general/projectphp1/index.php/welcome/`
* en eso la url base es `http://127.0.0.1/~general/projectphp1/`
* y el controlador es el depeus de "index.php" asi:

![x-guides-codeigniter-grocerycrud-debian-12-codeigniter-config-url.png](x-guides-codeigniter-grocerycrud-debian-12-codeigniter-config-url.png)

Para tener una nueva url de "request" en el proyecto hay que:

1. crear una clase nueva en controller 
2. la clase se llame igual que el archivo php nuevo
3. la **primera letra mayuscula, ojo**, 
4. y en este debe existir un metodo "index" siempre.
5. en el metodo index se puede simplemente escribir un "echo"
6. vemos en el navegador agregando despues de "index.php" `/nuevocontrol/`

entonces asumamos el archivo es "nuevocontrol.php" con este 
codigo:

```
<?php
class Nuevocontrol extends CI_Controller 
{
	public function index()
	{
		echo "Este es el nuevo conroller, nuevocontrol";
	}
}
```
Cada uno de los seis pasos anteriores se puede ver en la ilustacion:

![x-guides-codeigniter-grocerycrud-debian-18-usando-ci-geany-nuevocontrol.png](x-guides-codeigniter-grocerycrud-debian-18-usando-ci-geany-nuevocontrol.png)


## RESUMEN

En este documento realizamos las siguientes actividades:

1. Instalamos los programs para servir como servidor web de desarrollo 
en la misma maquina que desarrollaremos.
2. Los programas de servidor fueron `lighttpd` como servidor web, 
y `mariadb` como servidor de base de datos, adicinalmente se instalo 
el entorno `php` y `golang` asi como `gambas3`
3. Instalamos los programas para herramientas en la maquina de desarrollo 
para codificar que se muestra en el siguiente documento.
4. Las herramientas de desarrollo para codificar son `geany` y `gambas3`
como editor de texto, `mysql-workbench` como administrador de la 
base de datos, y `git` como gestor de control de versiones.
5. Configuramos el editor IDE `geany` para tener el proyecto abierto, 
navegar en sus archivos, y previsualizar sin usar navegador web.
